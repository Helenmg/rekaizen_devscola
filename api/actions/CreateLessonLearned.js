const LessonsLearnedService = require ('../lessonsLearned/service')

class CreateLessonLearned {
  static do(lessonLearned) {
    return LessonsLearnedService.create(lessonLearned.description, lessonLearned.issue)
  }
}

module.exports = CreateLessonLearned
