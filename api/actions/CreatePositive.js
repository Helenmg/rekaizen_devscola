const PositivesService = require ('../positives/service')

class CreatePositive {
  static do(positive) {
    return PositivesService.create(positive.description, positive.issue)
  }
}

module.exports = CreatePositive
