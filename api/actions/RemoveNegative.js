const NegativesService = require ('../negatives/service')

class RemoveNegative {
  static do(negative) {
    return NegativesService.remove(negative)
  }
}

module.exports = RemoveNegative
