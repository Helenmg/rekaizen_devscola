
const GoodPracticesService = require ('../goodPractices/service')

class RetrieveGoodPractices {
  static do(issue) {
    return GoodPracticesService.retrieveAll(issue)
  }
}

module.exports = RetrieveGoodPractices
