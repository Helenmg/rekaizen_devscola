const LessonsLearnedService = require ('../lessonsLearned/service')

class RetrieveLessonsLearned {
  static do(issue) {
    return LessonsLearnedService.retrieveAll(issue)
  }
}

module.exports = RetrieveLessonsLearned
