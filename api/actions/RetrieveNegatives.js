
const NegativesService = require ('../negatives/service')

class RetrieveNegatives {
  static do(issue) {
    return NegativesService.retrieveAll(issue)
  }
}

module.exports = RetrieveNegatives
