const TranslationsService = require ('../translations/service')

class TranslateAll {
  static do () {
    return TranslationsService.retrieveAll()
  }
}

module.exports = TranslateAll
