
class GoodPractice {
  constructor(description, issue){
    this.birthday = new Date().getTime()
    this.description = description
    this.issue = issue
  }

  serialize() {
    return {
      'description': this.description,
      'issue': this.issue,
      'birthday': this.birthday
    }
  }

  isOlderThan(goodPractice) {
    if(!(goodPractice instanceof GoodPractice)){throw new Error('Can not be compared with another type')}

    return goodPractice.birthday > this.birthday
  }

  belongsTo(issue) {
    return (this.issue == issue)
  }
}

module.exports = GoodPractice
