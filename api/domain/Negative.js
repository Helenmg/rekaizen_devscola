
class Negative {
  constructor(description, issue){
    this.birthday = new Date().getTime()
    this.description = description
    this.issue = issue
  }

  static from(document) {
    const negative = new Negative(
      document.description,
      document.issue)

    negative.birthday = document.birthday

    return negative
  }

  serialize() {
    return {
      'description': this.description,
      'issue': this.issue,
      'birthday': this.birthday
    }
  }

  isOlderThan(negative) {
    if(!(negative instanceof Negative)){throw new Error('Can not be compared with another type')}

    return negative.birthday > this.birthday
  }

  isEqualTo(negative) {
    return negative.birthday == this.birthday && negative.issue == this.issue
  }

  belongsTo(issue) {
    return (this.issue == issue)
  }
}

module.exports = Negative
