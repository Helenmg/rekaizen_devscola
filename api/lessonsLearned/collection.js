
let collection = []
class LessonsLearnedCollection {
  static create(lessonLearned) {

    collection.push(lessonLearned)

    return lessonLearned
  }

  static retrieveAll(issue) {
    const filtered = collection.filter((lessonLearned)=>{
      return lessonLearned.belongsTo(issue)
    })
    return filtered
  }

  static drop() {
    collection = []
  }
}

module.exports = LessonsLearnedCollection
