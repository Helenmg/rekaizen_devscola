
let collection = []
class PositivesCollection {
  static create(positive) {
    collection.push(positive)

    return positive
  }

  static retrieveAll(issue) {
    const filtered = collection.filter((positive)=>{
      return positive.belongsTo(issue)
    })
    return filtered
  }

  static remove(positive) {
    let found = collection.find((element) => {
      return positive.isEqualTo(element)
    })

    let index = collection.indexOf(found)
    collection.splice(index, 1)

    return found
  }

  static drop() {
    collection = []
  }
}

module.exports = PositivesCollection
