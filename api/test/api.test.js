const GoodPracticesCollection = require ('../goodPractices/collection')
const GoodPracticesService = require ('../goodPractices/service')
const PositivesService = require ('../positives/service')
const LessonsLearnedCollection = require ('../lessonsLearned/collection')
const LessonsLearnedService = require ('../lessonsLearned/service')
const PositivesCollection = require ('../positives/collection')

const chaiHttp = require('chai-http')
const expect = require('chai').expect
const api = require('../api')
const chai = require('chai')

chai.use(chaiHttp)

describe('Api', () => {
  beforeEach(() => {
    GoodPracticesCollection.drop()
    LessonsLearnedCollection.drop()
    PositivesCollection.drop()
  })

  it('retrieves translation', (done) => {
    chai.request(api)
      .post('/translate')
      .end((_, response) => {
        const translations = JSON.parse(response.text)

        expect(translations.rekaizen).to.eq('ReKaizen')
        done()
      })
  })

  it('creates a Good Practice for an Issue', (done) => {
    const goodPractice = {description: 'A new Good Practice', issue: 'an issue'}

    chai.request(api)
      .post('/createGoodPractice')
      .send(goodPractice)
      .end((_, response) => {
        const createdGoodPractice = JSON.parse(response.text)

        expect(createdGoodPractice.description).to.eq(goodPractice.description)
        expect(createdGoodPractice.issue).to.eq(goodPractice.issue)
        done()
      })
  })

  it('retrieves Good Practices for an issue', (done) => {
    const issue = {issue: 'an issue'}
    let goodPractices = createGoodPractices(issue.issue)
    createGoodPractices('another issue')

    chai.request(api)
      .post('/retrieveGoodPractices')
      .send(issue)
      .end((_, response) => {
        const retrievedGoodPractices = JSON.parse(response.text)

        expect(retrievedGoodPractices).to.deep.eq(goodPractices)
        done()
      })
  })

  it('creates a Lesson Learned for an issue', (done) => {
    const lessonLearned = {description: 'A new Lesson Learned', issue: 'an issue'}

    chai.request(api)
      .post('/createLessonLearned')
      .send(lessonLearned)
      .end((_, response) => {
        const createdLessonLearned = JSON.parse(response.text)

        expect(createdLessonLearned.description).to.eq(lessonLearned.description)
        done()
      })
  })

  it('retrieves Lessons Learned', (done) => {
    const issue = {issue: 'an issue'}
    let lessonsLearned = createLessonsLearned(issue.issue)
    createLessonsLearned('another issue')

    chai.request(api)
      .post('/retrieveLessonsLearned')
      .send(issue)
      .end((_, response) => {
        const retrieveLessonsLearned = JSON.parse(response.text)

        expect(retrieveLessonsLearned).to.deep.eq(lessonsLearned)
        done()
      })
  })

  it('creates a Positive', (done) => {
    const positive = {description: 'A new Positive'}

    chai.request(api)
      .post('/createPositive')
      .send(positive)
      .end((_, response) => {
        const createdPositive = JSON.parse(response.text)

        expect(createdPositive.description).to.eq(positive.description)
        done()
      })
  })

  it('retrieves Positives for an issue', (done) => {
    const issue = { issue: 'an issue'}
    let positives = createPositives(issue.issue)
    createPositives('another issue')

    chai.request(api)
      .post('/retrievePositives')
      .send(issue)
      .end((_, response) => {
        const retrievePositives = JSON.parse(response.text)

        expect(retrievePositives).to.deep.eq(positives)
        done()
      })
  })

  it('remove a Positive for an issue', (done) => {
    const positive = { description: 'a positive', issue: 'issue' }
    let createdPositive = createPositive(positive)

    chai.request(api)
      .post('/removePositive')
      .send(createdPositive)
      .end((_, response) => {
        const removePositive = JSON.parse(response.text)

        expect(removePositive).to.deep.eq(createdPositive)
        done()
      })
  })

  function createGoodPractices(issue) {
    let firstGoodPractice = {description: 'A new first Good Practice'}
    let secondGoodPractice = {description: 'A new second Good Practice'}
    firstGoodPractice = GoodPracticesService.create(firstGoodPractice, issue)
    firstGoodPractice.birthday += 1000
    secondGoodPractice = GoodPracticesService.create(secondGoodPractice, issue)

    return [firstGoodPractice, secondGoodPractice]
  }

  function createLessonsLearned(issue) {
    let firstLessonLearned = {description: 'A new first Good Practice'}
    let secondLessonLearned = {description: 'A new second Good Practice'}
    firstLessonLearned = LessonsLearnedService.create(firstLessonLearned, issue)
    firstLessonLearned.birthday += 1000
    secondLessonLearned = LessonsLearnedService.create(secondLessonLearned, issue)

    return [firstLessonLearned, secondLessonLearned]
  }

  function createPositives(issue) {
    let firstPositive = {description: 'A new first Positive'}
    let secondPositive = {description: 'A new second Positive'}
    firstPositive = PositivesService.create(firstPositive, issue)
    secondPositive = PositivesService.create(secondPositive, issue)

    return [firstPositive, secondPositive]
  }

  function createPositive(positive) {
    let createPositive = PositivesService.create(positive.description, positive.issue)

    return createPositive
  }
})
