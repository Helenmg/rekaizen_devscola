const expect = require('chai').expect

const GoodPractice = require ('../../domain/GoodPractice')

describe('Good Practice', () => {
  it('is timestamped at inicialization', () =>{
    const now = new Date().getTime()
    const aGoodPractice = new GoodPractice()
    const then = new Date().getTime()
    const birthday = aGoodPractice.serialize().birthday

    expect(birthday).to.gte(now)
    expect(birthday).to.lte(then)
  })

  it('needs a description', () =>{
    const description = "A description"
    const aGoodPractice = new GoodPractice(description)

    expect(aGoodPractice.serialize().description).to.eq(description)
  })

  it('can be serialized', () =>{
    const description = "A description"
    const aGoodPractice = new GoodPractice(description)

    expect(aGoodPractice.serialize()).to.have.keys(['description', 'issue', 'birthday'])
  })

  it('can be compared', () =>{
    const aGoodPractice = new GoodPractice()
    const otherGoodPractice = new GoodPractice()

    otherGoodPractice.birthday += 1000

    expect(aGoodPractice.isOlderThan(otherGoodPractice)).to.be.true
    expect(otherGoodPractice.isOlderThan(aGoodPractice)).to.be.false
  })

  it('can not be compared if is not a instance of class', () =>{
    const aGoodPractice = new GoodPractice()
    const otherGoodPractice = 'goodPractice'
    const secondGoodPractice = new GoodPractice(otherGoodPractice)

    expect(() => { secondGoodPractice.isOlderThan(otherGoodPractice)}).to.throw('Can not be compared with another type')
  })

  it('knows if belongs to a Issue', () => {
    const issue = 'aIssue'

    const goodPractice = new GoodPractice('aDescription', issue)

    expect(goodPractice.belongsTo(issue)).to.be.true
  })

  it('knows if does not belong to a Issue', () => {

    const goodPractice = new GoodPractice('aDescription', 'aIssue')

    expect(goodPractice.belongsTo('anotherIssue')).to.be.false
  })
})
