const expect = require('chai').expect

const LessonLearned = require ('../../domain/LessonLearned')

describe('Lesson Learned', () => {
  it('is timestamped at inicialization', () =>{
    const now = new Date().getTime()
    const aLessonLearned = new LessonLearned()
    const then = new Date().getTime()
    const birthday = aLessonLearned.serialize().birthday

    expect(birthday).to.gte(now)
    expect(birthday).to.lte(then)
  })

  it('needs a description', () =>{
    const description = "A description"
    const aLessonLearned = new LessonLearned(description)

    expect(aLessonLearned.serialize().description).to.eq(description)
  })

  it('can be serialized', () =>{
    const description = "A description"
    const aLessonLearned = new LessonLearned(description)

    expect(aLessonLearned.serialize()).to.have.keys(['description', 'birthday', 'issue'])
  })

  it('can be compared', () =>{
    const aLessonLearned = new LessonLearned()
    const otherLessonLearned = new LessonLearned()

    otherLessonLearned.birthday += 1000

    expect(aLessonLearned.isOlderThan(otherLessonLearned)).to.be.true
    expect(otherLessonLearned.isOlderThan(aLessonLearned)).to.be.false
  })

  it('can not be compared if is not a instance of class', () =>{
    const aLessonLearned = new LessonLearned()
    const otherLessonLearned = 'lessonLearned'
    const secondLessonLearned = new LessonLearned(otherLessonLearned)

    expect(() => { secondLessonLearned.isOlderThan(otherLessonLearned)}).to.throw('Can not be compared with another type')
  })

  it('knows if belongs to a Issue', () => {
    const issue = 'aIssue'

    const lessonLearned = new LessonLearned('aDescription', issue)

    expect(lessonLearned.belongsTo(issue)).to.be.true
  })

  it('knows if does not belong to a Issue', () => {

    const lessonLearned = new LessonLearned('aDescription', 'aIssue')

    expect(lessonLearned.belongsTo('anotherIssue')).to.be.false
  })
})
