const expect = require('chai').expect

const Negative = require ('../../domain/Negative')

describe('Negative', () => {
  it('is timestamped at inicialization', () =>{
    const now = new Date().getTime()
    const aNegative = new Negative()
    const then = new Date().getTime()
    const birthday = aNegative.serialize().birthday

    expect(birthday).to.gte(now)
    expect(birthday).to.lte(then)
  })

  it('needs a description', () =>{
    const description = "A description"
    const aNegative = new Negative(description)

    expect(aNegative.serialize().description).to.eq(description)
  })

  it('can be serialized', () =>{
    const description = "A description"
    const aNegative = new Negative(description)

    expect(aNegative.serialize()).to.have.keys(['description', 'issue', 'birthday'])
  })

  it('can be compared', () =>{
    const aNegative = new Negative()
    const otherNegative = new Negative()

    otherNegative.birthday += 1000

    expect(aNegative.isOlderThan(otherNegative)).to.be.true
    expect(otherNegative.isOlderThan(aNegative)).to.be.false
  })

  it('can not be compared if is not a instance of class', () =>{
    const aNegative = new Negative()
    const otherNegative = 'lessonLearned'
    const secondNegative = new Negative(otherNegative)

    expect(() => { secondNegative.isOlderThan(otherNegative)}).to.throw('Can not be compared with another type')
  })

  it('knows if is equal to another negative', () =>{
    const aNegative = new Negative()
    const otherNegative = new Negative()

    otherNegative.birthday = aNegative.birthday

    expect(aNegative.isEqualTo(otherNegative)).to.be.true
  })

  it('knows if belongs to a Issue', () => {
    const issue = 'aIssue'

    const negative = new Negative('aDescription', issue)

    expect(negative.belongsTo(issue)).to.be.true
  })

  it('knows if does not belong to a Issue', () => {

    const negative = new Negative('aDescription', 'aIssue')

    expect(negative.belongsTo('anotherIssue')).to.be.false
  })
})
