const expect = require('chai').expect

const Positive = require ('../../domain/Positive')

describe('Positive', () => {
  it('is timestamped at inicialization', () =>{
    const now = new Date().getTime()
    const aPositive = new Positive()
    const then = new Date().getTime()
    const birthday = aPositive.serialize().birthday

    expect(birthday).to.gte(now)
    expect(birthday).to.lte(then)
  })

  it('needs a description', () =>{
    const description = "A description"
    const aPositive = new Positive(description)

    expect(aPositive.serialize().description).to.eq(description)
  })

  it('can be serialized', () =>{
    const description = "A description"
    const aPositive = new Positive(description)

    expect(aPositive.serialize()).to.have.keys(['description', 'issue', 'birthday'])
  })

  it('can be compared', () =>{
    const aPositive = new Positive()
    const otherPositive = new Positive()

    otherPositive.birthday += 1000

    expect(aPositive.isOlderThan(otherPositive)).to.be.true
    expect(otherPositive.isOlderThan(aPositive)).to.be.false
  })

  it('can not be compared if is not a instance of class', () =>{
    const aPositive = new Positive()
    const otherPositive = 'lessonLearned'
    const secondPositive = new Positive(otherPositive)

    expect(() => { secondPositive.isOlderThan(otherPositive)}).to.throw('Can not be compared with another type')
  })

  it('knows if is equal to another positive', () =>{
    const aPositive = new Positive()
    const otherPositive = new Positive()

    otherPositive.birthday = aPositive.birthday

    expect(aPositive.isEqualTo(otherPositive)).to.be.true
  })

  it('knows if belongs to a Issue', () => {
    const issue = 'aIssue'

    const positive = new Positive('aDescription', issue)

    expect(positive.belongsTo(issue)).to.be.true
  })

  it('knows if does not belong to a Issue', () => {

    const positive = new Positive('aDescription', 'aIssue')

    expect(positive.belongsTo('anotherIssue')).to.be.false
  })
})
