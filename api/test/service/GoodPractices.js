const expect = require('chai').expect

const GoodPracticesService = require ('../../goodPractices/service')
const GoodPracticesCollection = require ('../../goodPractices/collection')
const GoodPractice = require ('../../domain/GoodPractice')


describe('Good Practices service', () => {
  beforeEach(()=>{
    GoodPracticesCollection.drop()
  })

  it('retrieves good practices in ascendent order', () =>{
    const firstGoodPractice = new GoodPractice()
    const secondGoodPractice = new GoodPractice()
    secondGoodPractice.birthday += 10000
    GoodPracticesCollection.create(firstGoodPractice)
    GoodPracticesCollection.create(secondGoodPractice)

    let retrieved = GoodPracticesService.retrieveAll()

    expect(retrieved[0].birthday).to.eq(secondGoodPractice.birthday)
  })

  it('retrieves good practices for an issue', () => {
    const description = 'a description'
    const issue = 'an issue'
    const goodPracticeForAnIssue = new GoodPractice(description, issue)
    const goodPracticeForAnotherIssue = new GoodPractice('another description', 'another issue')
    GoodPracticesCollection.create(goodPracticeForAnIssue)
    GoodPracticesCollection.create(goodPracticeForAnotherIssue)

    let retrieved = GoodPracticesService.retrieveAll(issue)

    expect(retrieved.length).to.eq(1)
    expect(retrieved[0].description).to.eq(description)
  })
})
