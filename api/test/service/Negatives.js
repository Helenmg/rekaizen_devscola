const expect = require('chai').expect

const NegativesService = require ('../../negatives/service')
const NegativesCollection = require ('../../negatives/collection')
const Negative = require ('../../domain/Negative')


describe('Negatives service', () => {
  beforeEach(()=>{
    NegativesCollection.drop()
  })
  it('retrieves Negatives in descendent order', () =>{
    const firstNegative = new Negative()
    const secondNegative = new Negative()
    secondNegative.birthday += 10000
    NegativesCollection.create(firstNegative)
    NegativesCollection.create(secondNegative)

    let retrieved = NegativesService.retrieveAll()

    expect(retrieved[0].birthday).to.eq(firstNegative.birthday)
  })

  it('retrieves Negatives for an issue', ()=>{
    const description = 'a description'
    const issue = 'an issue'
    const negativeForAnIssue = new Negative(description, issue)
    const negativeForAnotherIssue = new Negative('another description', 'another issue')
    NegativesCollection.create(negativeForAnIssue)
    NegativesCollection.create(negativeForAnotherIssue)

    let retrieved = NegativesService.retrieveAll(issue)

    expect(retrieved.length).to.eq(1)
    expect(retrieved[0].description).to.eq(description)
  })

  it('removes a Negative from negative list', ()=>{
    const description = 'a description'
    const issue = 'an issue'
    const negativeForAnIssue = new Negative(description, issue)
    const secondNegativeForAnIssue = new Negative('second description', issue)
    const thirdNegativeForAnIssue = new Negative('third description', issue)
    const fourthNegativeForAnIssue = new Negative('fourth description', issue)
    NegativesCollection.create(negativeForAnIssue)
    NegativesCollection.create(secondNegativeForAnIssue)
    NegativesCollection.create(thirdNegativeForAnIssue)
    NegativesCollection.create(fourthNegativeForAnIssue)
    secondNegativeForAnIssue.birthday += 10000
    thirdNegativeForAnIssue.birthday += 20000
    fourthNegativeForAnIssue.birthday += 30000

    let removed = NegativesService.remove(thirdNegativeForAnIssue)
    let retrieved = NegativesService.retrieveAll(issue)

    expect(retrieved.length).to.eq(3)
    expect(retrieved[0].description).to.eq(negativeForAnIssue.description)
    expect(retrieved[1].description).to.eq(secondNegativeForAnIssue.description)
    expect(retrieved[2].description).to.eq(fourthNegativeForAnIssue.description)
  })
})
