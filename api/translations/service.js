const TranslationsCollection = require ('./collection')

class TranslationsService {
  static retrieveAll () {
    return TranslationsCollection.retrieveAll()
  }
}

module.exports = TranslationsService;
