import React from 'react'

class ButtonAdd extends React.Component {
  render() {
    return (
      <a className="button" onClick={this.props.onClick} id={"new-" + this.props.id}>{this.props.text}</a>
    )
  }
}

ButtonAdd.defaultProps = {
  id: '',
  onClick: () => {},
  text: ''
}

export default ButtonAdd
