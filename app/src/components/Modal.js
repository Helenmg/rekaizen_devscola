import React from 'react'

class Modal extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      value: '',
      disabled: true
    }
  }

  hide() {
    this.clearText()
    this.props.hideModal()
    this.setState({ disabled: true })
  }

  addGoodPractice(event) {
    let notIsSpacePressed = (event.key != ' ')
    let notIsEnterPressed = (event.key != 'Enter')
    let notIsClickPressed = (event.type != 'click')

    if(notIsSpacePressed && notIsEnterPressed && notIsClickPressed) { return }
    if(this.state.disabled) { return }

    if (this.state.value != ''){
      this.props.create(this.state.value)
    }

    this.hide()
  }

  clearText() {
    this.setState({ value: '' })
  }

  onChange(event) {
    this.setState({value: event.target.value})
  }

  confirm() {
    this.setState({ disabled: !this.state.disabled })
  }

  render() {
    if (!this.props.showModal) { return(<div></div>) }
    return (
      <div
        id={"modal-add-" + this.props.id}
        className="modal">
        <div className="modal-content">
          <div className="card-title">
            <h5>{this.props.modalTitle}</h5>
          </div>
          <div className="card-block">
            <div className='alert'>
              <em className="fa fa-bullhorn"></em>
              {this.props.explanation}
            </div>
            <textarea
              className="text-border"
              onChange={this.onChange.bind(this)}
              value={this.state.value}
              id={this.props.id + "-text"}
              autoFocus="true"
              tabindex="-1">
            </textarea>
            <div>
              <label
              className="label-confirmation"
              for="confirm-declaration">
                <input
                  type="checkbox"
                  className="confirmation-checkbox"
                  id="confirm-declaration"
                  onClick={this.confirm.bind(this)}
                  tabindex="-2"
                />
                {' ' + this.props.confirmation}
              </label>
            </div>
            <div class="float-right">
              <a
                className='button'
                onClick={this.addGoodPractice.bind(this)}
                id={"add-" + this.props.id}
                disabled={this.state.disabled}
                tabindex="0"
                onKeyPress={this.addGoodPractice.bind(this)}>
                {this.props.add}
              </a>
            </div>
            <a
              className="button cancel"
              onClick={this.hide.bind(this)}>
              {this.props.cancel}
            </a>
          </div>
        </div>
      </div>
    )
  }
}

Modal.defaultProps = {
  event: '',
  id: '',
  add: '',
  cancel: '',
  explanation: '',
  modalTitle: '',
  confirmation: '',
  create: () => {},
  showModal: false
}

export default Modal
