import TextArea from './TextArea'
import LinkAdd from './LinkAdd'
import React from 'react'

class SimpleList extends React.Component {
  onClick(item, event) {
    this.props.remove(item)

    event.preventDefault()
  }

  removeButton(item) {
    return (
      <a
        href="#"
        className="float-right trash-icon"
        onClick={this.onClick.bind(this, item)}>
        <em className="fa fa-trash"></em>
      </a>
    )
  }

  rows(){
    let rows = []
    this.props.collection.forEach((item, index) =>{
      if(item.description != '' && item.description != undefined ){
        rows.push(
          <tr key={index}>
            <td className="white-td">
              {this.removeButton(item)}
              <p className="padding-left">
                {item.description}
              </p>
            </td>
          </tr>
        )
      }
    })
    return rows
  }

  render() {
    return (
    <div>
      <table className="white-table" id={this.props.id + "-list"}>
        <thead>
          <tr>
            <th>
              <h5 className="table-title">
                {this.props.listTitle}
              </h5>
            </th>
          </tr>
        </thead>
        <tbody>
          {this.rows()}
        </tbody>
      </table>
      <TextArea
        create={this.props.create}
        show={this.props.show}
        hide={this.props.hide}
        id={this.props.id}
      />
      <div className="container-link">
        <LinkAdd
        onClick={this.props.onClick}
        text={this.props.add}
        id={this.props.id}
        />
      </div>
    </div>
    )
  }
}

SimpleList.defaultProps = {
  collection: [],
  listTitle: '',
  id: '',
  onClick: () => {},
  add: ''
}

export default SimpleList
