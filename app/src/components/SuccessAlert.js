import React from 'react'

class SuccessAlert extends React.Component {
  render() {
    if (!this.props.show) { return(<div></div>) }

    return (
      <div className="alert background-success" id='success-alert'>
        <em className="fa fa-thumbs-up"></em> {this.props.text}
      </div>
    )
  }
}

SuccessAlert.defaultProps = {
  show: false,
  text: ''
}

export default SuccessAlert
