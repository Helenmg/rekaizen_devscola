import React from 'react'
import ReactDOM from 'react-dom'
import {Bus} from '../infrastructure/bus'
import Navbar from './Navbar'
import GoodPractices from './GoodPractices'
import LessonsLearned from './LessonsLearned'
import Positives from './Positives'
import Negatives from './Negatives'


class Issue {
  constructor(){
    this.state = {
      translations: {},
      goodPractices: [],
      lessonsLearned: [],
      positives: [],
      negatives: []
    }

    this.subscriptions()
    this.issue = this.retrieveIssueFromUrl()
    this.askCollections()
  }

  subscriptions() {
    Bus.subscribe("retrieved.goodPractices", this.updateGoodPractices.bind(this) )
    Bus.subscribe("retrieved.lessonsLearned", this.updateLessonsLearned.bind(this) )
    Bus.subscribe("retrieved.positives", this.updatePositives.bind(this) )
    Bus.subscribe("retrieved.negatives", this.updateNegatives.bind(this) )
    Bus.subscribe("removed.positive", this.askCollections.bind(this) )
    Bus.subscribe("removed.negative", this.askCollections.bind(this) )
    Bus.subscribe('created.goodPractice', this.askCollections.bind(this))
    Bus.subscribe('created.lessonLearned', this.askCollections.bind(this))
    Bus.subscribe("created.positive", this.askCollections.bind(this))
    Bus.subscribe("created.negative", this.askCollections.bind(this))
    Bus.subscribe("got.translations", this.updateTranslations.bind(this))
  }

  retrieveIssueFromUrl(){
    const params = new URLSearchParams(window.location.search)
    return params.get("issue")
  }


  askCollections() {
    Bus.publish("retrieve.goodPractices", {issue: this.issue})
    Bus.publish("retrieve.lessonsLearned", {issue: this.issue})
    Bus.publish("retrieve.positives", {issue: this.issue})
    Bus.publish("retrieve.negatives", {issue: this.issue})
  }

  updateGoodPractices(payload) {
    this.updateState({ goodPractices: payload })
  }

  updateLessonsLearned(payload) {
    this.updateState({ lessonsLearned: payload })
  }

  updatePositives(payload) {
    this.updateState({ positives: payload })
  }

  updateNegatives(payload) {
    this.updateState({ negatives: payload })
  }

  updateTranslations(payload) {
    this.updateState({ translations: payload })
  }

  updateState(value) {
    this.state = Object.assign(this.state, value)
    this.render()
  }

  createPositive(value) {
    let payload = { description: value, issue: this.issue }
    Bus.publish("create.positive", payload)
  }

  createNegative(value) {
    let payload = { description: value, issue: this.issue }
    Bus.publish("create.negative", payload)
  }

  createGoodPractice(value) {
    let payload = { description: value, issue: this.issue }
    Bus.publish("create.goodPractice", payload)
  }

  createLessonLearned(value) {
    let payload = { description: value, issue: this.issue }
    Bus.publish("create.lessonLearned", payload)
  }

  removePositive(positive) {
    Bus.publish('remove.positive', positive)
  }

  removeNegative(negative) {
    Bus.publish('remove.negative', negative)
  }

  render() {
    ReactDOM.render(
      <div className="row maximum-height">
        <div className="column-height">
          <Navbar/>
        </div>
        <section id="main-content" className="column maximum-height">
          <div className="row grid-responsive maximum-height">
            <div className="col-50">
              <div className="col-50-50">
                <Positives
                  id={"positive"}
                  listTitle={this.state.translations.positives}
                  add={this.state.translations.addPositive}
                  collection={this.state.positives}
                  create={this.createPositive.bind(this)}
                  remove={this.removePositive.bind(this)}
                  />
              </div>
              <div className="col-50-50">
                <Negatives
                  id={"negative"}
                  listTitle={this.state.translations.negatives}
                  add={this.state.translations.addNegative}
                  collection={this.state.negatives}
                  create={this.createNegative.bind(this)}
                  remove={this.removeNegative.bind(this)}
                  />
              </div>
            </div>
            <LessonsLearned
              {...this.state}
              create={this.createLessonLearned.bind(this)}
              />
            <GoodPractices
              {...this.state}
              create={this.createGoodPractice.bind(this)}
            />
          </div>
        </section>
      </div>,
      document.querySelector('#issue')
    )
  }
}

export default Issue
