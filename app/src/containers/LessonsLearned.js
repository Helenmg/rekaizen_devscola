import React from 'react'
import Column from '../components/Column'

class LessonsLearned extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      showModal: false
    }
  }

  showModal() {
    this.setState({showModal: true})
  }

  hideModal() {
    this.setState({showModal: false})
  }

  render() {
    return(
      <Column
        id={"lesson-learned"}
        add={this.props.translations.add}
        cancel={this.props.translations.cancel}
        explanation={this.props.translations.lessonLearnedExplanation}
        modalTitle={this.props.translations.addLessonLearned}
        confirmation={this.props.translations.lessonLearnedConfirmation}
        listTitle={this.props.translations.lessonsLearned}
        openModal={this.showModal.bind(this)}
        showModal={this.state.showModal}
        hideModal={this.hideModal.bind(this)}
        create={this.props.create}
        collection={this.props.lessonsLearned}
      />
    )
  }
}

export default LessonsLearned
