import React from 'react'
import {Bus} from '../infrastructure/bus'
import SuccessAlert from '../components/SuccessAlert'

class Navbar extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      title: '',
      goodPracticeText: '',
      lessonLearnedText: '',
      text: '',
      showAlert: false
    }
  }

  componentDidMount() {
    Bus.subscribe("got.translations", this.setTranslations.bind(this))
    Bus.subscribe('created.goodPractice', this.showAlertGoodPractice.bind(this))
    Bus.subscribe('created.lessonLearned', this.showAlertLessonLearned.bind(this))
  }


  setTranslations(payload) {
    this.setState({
      title: payload.rekaizen,
      goodPracticeText: payload.goodPracticeAdded,
      lessonLearnedText: payload.lessonLearnedAdded
    })
  }

  showAlertGoodPractice() {
    this.setState({
      showAlert: true,
      text: this.state.goodPracticeText
    })
    this.hideAlert()
  }

  showAlertLessonLearned() {
    this.setState({
      showAlert: true,
      text: this.state.lessonLearnedText
    })
    this.hideAlert()
  }

  hideAlert() {
    setTimeout(() => {
      this.setState({ showAlert: false })
    }, 5000)
  }


  render() {
      return (
        <div className="navbar">
      		<div className="row">
      			<div className="column column-30 col-site-title">
      				<a href="#" className="site-title float-left" id="rekaizen">{this.state.title}</a>
      			</div>
      			<div className="column column-40 col-search">
              <SuccessAlert text={this.state.text} show={this.state.showAlert}/>
      			</div>
      			<div className="column column-30"></div>
      		</div>
      	</div>
      )
    }
  }

export default Navbar
