import SimpleList from '../components/SimpleList'
import React from 'react'

class Positives extends React.Component {
  constructor(props){
    super(props)

    this.state = {
      showTextArea: false
    }
  }

  hideTextArea(){
    this.setState({showTextArea: false})
  }

  showTextArea() {
    this.setState({showTextArea: true})
  }

  render() {
    return (
      <div>
        <SimpleList {...this.props}
          onClick={this.showTextArea.bind(this)}
          create={this.props.create}
          show={this.state.showTextArea}
          hide={this.hideTextArea.bind(this)}
          remove={this.props.remove}
          />
      </div>
    )
  }
}

export default Positives
