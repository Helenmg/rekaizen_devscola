import React from 'react'
import ReactDOM from 'react-dom'
import {Bus} from './infrastructure/bus'

import Issue from './containers/Issue'

import Translations from './services/translations'
import GoodPractices from './services/goodPractices'
import LessonsLearned from './services/lessonsLearned'
import Positives from './services/positives'
import Negatives from './services/negatives'

new Translations()
new GoodPractices()
new LessonsLearned()
new Positives()
new Negatives()

new Issue().render()
