import {Bus} from '../infrastructure/bus'
import {APIClient} from '../infrastructure/apiClient'


export default class LessonsLearned {
  constructor() {
    this.client = APIClient

    this.subscriptions()
  }

  subscriptions() {
    Bus.subscribe('create.lessonLearned', this.create.bind(this))
    Bus.subscribe('retrieve.lessonsLearned', this.retrieveAllFor.bind(this))
  }

  create(payload) {
    let callback = this.buildCallback('created.lessonLearned')
    let body = payload
    let url = 'createLessonLearned'
    this.client.hit(url, body, callback)
  }

  retrieveAllFor(payload){
    let callback = this.buildCallback('retrieved.lessonsLearned')
    let body = payload
    let url = 'retrieveLessonsLearned'
    this.client.hit(url, body, callback)
  }

  buildCallback(signal) {
    return function(response) {
      Bus.publish(signal, response)
    }
  }
}
