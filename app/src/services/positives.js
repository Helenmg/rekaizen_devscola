import {Bus} from '../infrastructure/bus'
import {APIClient} from '../infrastructure/apiClient'


export default class Positives {
  constructor() {
    this.client = APIClient

    this.subscriptions()
  }

  subscriptions() {
    Bus.subscribe('create.positive', this.create.bind(this))
    Bus.subscribe('remove.positive', this.removePositive.bind(this))
    Bus.subscribe('retrieve.positives', this.retrieveAllFor.bind(this))
  }

  create(payload) {
    let callback = this.buildCallback('created.positive')
    let body = payload
    let url = 'createPositive'
    this.client.hit(url, body, callback)
  }

  retrieveAllFor(payload){
    let callback = this.buildCallback('retrieved.positives')
    let body = payload
    let url = 'retrievePositives'
    this.client.hit(url, body, callback)
  }

  removePositive(payload) {
    let callback = this.buildCallback('removed.positive')
    let body = payload
    let url = 'removePositive'
    this.client.hit(url, body, callback)
  }

  buildCallback(signal) {
    return function(response) {
      Bus.publish(signal, response)
    }
  }
}
