import LessonsLearned from '../../src/services/lessonsLearned'
import Subscriber from './subscriber'
import {Bus} from '../../src/infrastructure/bus'
import StubAPI from './stubApi'

describe('Lessons Learned Service', () => {
  it('creates a lesson learned', () => {
    const subscriber = new Subscriber('created.lessonLearned')
    const service = new LessonsLearned()
    service.client = new StubAPI()

    Bus.publish('create.lessonLearned', { description: 'A Lesson Learned' })

    expect(subscriber.hasBeenCalled).toBeTruthy()
  })

  it('retrieves the lessons learned', () => {
    const subscriber = new Subscriber('retrieved.lessonsLearned')
    const service = new LessonsLearned()
    service.client = new StubAPI()

    Bus.publish('retrieve.lessonsLearned')

    expect(subscriber.hasBeenCalled).toBeTruthy()
  })
})
