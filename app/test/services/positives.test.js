import Positives from '../../src/services/positives'
import Subscriber from './subscriber'
import {Bus} from '../../src/infrastructure/bus'
import StubAPI from './stubApi'

describe('Positives Service', () => {
  it('creates a positive', () => {
    const subscriber = new Subscriber('created.positive')
    const service = new Positives()
    service.client = new StubAPI()

    Bus.publish('create.positive', { description: 'A Positive' })

    expect(subscriber.hasBeenCalled).toBeTruthy()
  })

  it('retrieves the positives', () => {
    const subscriber = new Subscriber('retrieved.positives')
    const service = new Positives()
    service.client = new StubAPI()

    Bus.publish('retrieve.positives')

    expect(subscriber.hasBeenCalled).toBeTruthy()
  })
})
