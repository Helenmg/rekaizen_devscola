import {Bus} from '../../src/infrastructure/bus'

class Subscriber {
  constructor(event) {
    this.hasBeenCalled = false
    this.payload = ''
    Bus.subscribe(event, this.subscription.bind(this))
  }

  subscription(payload) {
    this.hasBeenCalled = true
    this.payload = payload
  }

  hasBeenCalledWith(){
    return this.payload
  }
}

export default Subscriber
