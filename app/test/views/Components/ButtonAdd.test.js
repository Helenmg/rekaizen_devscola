import ButtonAdd from '../../../src/components/ButtonAdd'
import TestRenderer from 'react-test-renderer'
import Callback from './CallBack'
import React from 'react'

describe('ButtonAdd', () => {
  it('executes any callback when is clicked', () => {
    const callBack = new Callback()
    const buttonAdd = TestRenderer.create(<ButtonAdd onClick={callBack.toBeCalled.bind(callBack)} />).getInstance()

    buttonAdd.props.onClick()

    expect(callBack.hasBeenCalled()).toBeTruthy()
  })
})
