import LinkAdd from '../../../src/components/LinkAdd'
import TestRenderer from 'react-test-renderer'
import Callback from './CallBack'
import React from 'react'

describe('LinkAdd', () => {
  it('executes any callback when is clicked', () => {
    const callBack = new Callback()
    const linkAdd = TestRenderer.create(<LinkAdd onClick={callBack.toBeCalled.bind(callBack)} />).getInstance()

    linkAdd.props.onClick()

    expect(callBack.hasBeenCalled()).toBeTruthy()
  })
})
