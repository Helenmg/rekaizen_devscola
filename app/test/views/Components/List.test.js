import List from '../../../src/components/List'
import TestRenderer from 'react-test-renderer'
import React from 'react'

describe('List', () => {
  it('prints a collection', () => {
    const textToPrint = "description"
    const collection = [{description: textToPrint}]

    const list = TestRenderer.create(<List collection={collection}/>).toJSON()

    expect(content(list)).toEqual(textToPrint)
  })

  it('prints nothing for an empty collection', () => {
    const emptyCollection = []

    const list = TestRenderer.create(<List collection={emptyCollection}/>).toJSON()

    expect(contentForEmpty(list)).toBeNull()
  })

  function contentForEmpty(list){
    return list.children[1].children[0].children
  }

  function content(list){
    return list.children[1].children[0].children[0].children[0].children[0].children[0]
  }
})
