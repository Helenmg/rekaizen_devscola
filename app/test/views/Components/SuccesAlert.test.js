import SuccessAlert from '../../../src/components/SuccessAlert'
import TestRenderer from 'react-test-renderer'
import React from 'react'

describe('SuccessAlert', () => {
  it('starts hidden by default', () => {

    const successAlert = TestRenderer.create(<SuccessAlert  />).getInstance()

    expect(successAlert.props.show).toBeFalsy()
  })

  it('shows a text', () => {
    const aText = 'any text'

    const successAlert = TestRenderer.create(<SuccessAlert show={true} text={aText}  />).toJSON()

    const alertText = successAlert.children[2]
    expect(alertText).toEqual(aText)
  })
})
