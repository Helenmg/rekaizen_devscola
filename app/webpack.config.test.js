const path = require('path');
const webpack = require('webpack');

module.exports = {
  mode: 'none',
  entry: './src/main.js',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      }
    ]
   },
   resolve: {
     extensions: ['*', '.js', '.jsx']
   },
  output: {
    path: path.resolve(__dirname, 'public/dist'),
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.EnvironmentPlugin({
      'API_URL': 'http://api:3001/'
    })
  ]
};
