#/bin/sh

#Deploying API
cd api
git init
git add -A
git commit -m "New deploy"
heroku git:remote -a rekaizen-api
git push heroku master -f

#Moving to main project folder
cd ..

#Deploying APP
cd app
git init
git add -A
git commit -m "New deploy"
heroku git:remote -a rekaizen
git push heroku master -f

#Moving to main project folder
cd ..
