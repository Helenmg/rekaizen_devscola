import aja from 'aja'

class Fixtures {
  static cleanCollections() {
    aja()
      .method('post')
      .url('http://api:3001/clean')
      .go()
  }
}

export default Fixtures
