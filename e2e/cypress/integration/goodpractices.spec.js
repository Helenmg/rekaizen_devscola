import Fixtures from './Fixtures'
import Issue from './pages/Issue'

describe('Rekaizen', () => {
  beforeEach(() => {
    Fixtures.cleanCollections()
  })

  afterEach(() => {
    Fixtures.cleanCollections()
  })

  it('creates a Good Practice', () => {
    const description = "good practice text"

    const issue = new Issue()
      .openNewGoodPractice()
      .fillGoodPracticeWith(description)
      .confirmDeclaration()
      .addGoodPractice()

    expect(issue.includes('¡Habéis añadido una Buena Practica!')).to.be.true
  })

  it('can not add an undeclared Good Practice', () => {

    const issue = new Issue()
      .openNewGoodPractice()

    expect(issue.canAddGoodPractice()).to.be.false
  })

  it('shows a Good Practices list', () => {
    const aGoodPractice = 'first'

    const issue = new Issue().withAGoodPractice(aGoodPractice)

    expect(issue.includes(aGoodPractice)).to.be.true
  })
})
