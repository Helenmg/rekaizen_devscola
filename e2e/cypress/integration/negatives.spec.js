import Issue from './pages/Issue'
import Fixtures from './Fixtures'

describe('Rekaizen', () => {
  beforeEach(() => {
    Fixtures.cleanCollections()
  })

  afterEach(() => {
    Fixtures.cleanCollections()
  })

  it('creates a Negative', () => {
    const description = "negative text"

    const issue = new Issue()
      .openNewNegative()
      .fillNegativeWith(description)
      .confirmNegativeWithEnter()

    expect(issue.includes(description)).to.be.true
  })
})
