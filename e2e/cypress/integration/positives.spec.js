import Issue from './pages/Issue'
import Fixtures from './Fixtures'

describe('Rekaizen', () => {
  beforeEach(() => {
    Fixtures.cleanCollections()
  })

  afterEach(() => {
    Fixtures.cleanCollections()
  })

  it('creates a Positive', () => {
    const description = "positive text"

    const issue = new Issue()
      .openNewPositive()
      .fillPositiveWith(description)
      .confirmPositiveWithEnter()

    expect(issue.includes(description)).to.be.true
  })
})
